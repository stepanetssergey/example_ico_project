var MoniContract = artifacts.require("./MoniContract.sol");
var Token = artifacts.require("./MONIToken.sol");
var axios = require('axios');
require('datejs');

module.exports = deployer => {
  var owner = web3.eth.accounts[0];
  var date_before = Math.round(new Date().getTime()/1000);
  console.log("Date for contract:",date_before);

  return deployer.deploy(Token,
                         18,
                         10000000000000,
                         "MONI",
                         "MNT",
                          date_before+1,
                         {from: owner}).then(function() {
    console.log("MONI token address:" + Token.address);
  return deployer.deploy(MoniContract,Token.address, { from: owner }).then(function() {
    console.log("MoniContract address: "+ MoniContract.address );
   //  axios.post('http://127.0.0.1:8000/api/address', {
   //    contract_address: MoniContract.address,
   //    token_address: MoniContract.address
   // });

  });
});
};
