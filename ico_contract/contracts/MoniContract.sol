pragma solidity ^0.4.24;

import "./datatime.sol";
import "./MONIToken.sol";



contract MoniContract is DateTime {
// DECLARATION OF EVENTS ======================= start =========================
event Payment( string _bitcoin,
                   address _from,
                   uint _date,
                   uint _ether_amount,
                   uint _btc_amount,
                   uint _tokens,
                   uint _round,
                   string _type);

event SetInvestor(address _investor,
                  bool _status);
event OverLimit(address _investor,
                uint _date,
                uint _over_limit);
event SetInvestorKyc(address _investor_address,
                      bool  _extended_kyc);

// DECLARATION OF VARIABLES ==================== start =========================
    constructor(address _token_address) public {
        owner = msg.sender;
        token_address = _token_address;
        set_stage_initial();
        set_admin_initial();
        set_owners_initial();
    }

//VARIABLES
address owner;
uint public current_stage;
uint public stage_count;
address public token_address;
uint public token_to_cent = 10; //tokens to cent
uint public token_rate = 6000; //rate for ether to tokens
uint public bitcoin_rate = 100000; //rate for bitcoin to tokens
//uint public current_date = now;
uint public calculate_date;
uint public current_date;
bool public ico_end;
uint public end_ico_year;
uint public end_ico_month;
uint public end_ico_day;
uint public stop_contract_state;
uint public vesting_date_count;
uint public bonus_count;
uint public stage_in_recalculation;
uint public dollar_limit_notext_kyc = 17999;
uint public dollar_limit_ext_kyc = 1000000;
MONIToken public MONIcoin;
uint public general_limit_stage = 1000000 * (1 ether);
//HARD CODING

function get_current_date() internal returns (uint) {
  return current_date;
}

function set_general_limit(uint _general_limit_stage) public OnlyAdmin(msg.sender) {
  general_limit_stage = _general_limit_stage * (1 ether);
}

// Investors struct - write here when signup of investor
        struct Investors {
            uint total_invest;
            uint total_invest_ether;
            uint total_invest_btc;
            bool active;
            uint locked_amount;
            uint unlocked_amount;
            uint total_withdraw;
            uint signup;
            bool kyc;
            bool extended_kyc;
            uint total_original_invest;

        }

        mapping(address => Investors) public _investors;

//struct for stage

    struct stage {
        bool active;
        uint start_date;
        uint bonus; //bonus in percent
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
        uint end_date;
        uint total_investors;

    }

    mapping(uint=>stage) public _stage;
function set_stage_initial() internal {
_stage[1].vesting_time =9;
_stage[1].bonus = 0;
_stage[1].total_tokens = 3125000 * (1 ether);
_stage[1].max = 1000000;
_stage[1].min = 100000;

_stage[2].bonus = 30;
_stage[2].vesting_time = 9;
_stage[2].total_tokens = 12500000 * (1 ether);
_stage[2].max = 1000000;
_stage[2].min = 50000;

_stage[3].bonus = 20;
_stage[3].vesting_time = 9;
_stage[3].total_tokens = 25000000 * (1 ether);
_stage[3].max = 1000000;
_stage[3].min = 20000;

_stage[4].bonus = 15;
_stage[4].vesting_time = 6;
_stage[4].total_tokens = 12500000 * (1 ether);
_stage[4].min = 1000;
_stage[4].max = 15000;

_stage[5].bonus = 10;
_stage[5].vesting_time = 6;
_stage[5].total_tokens = 18750000 * (1 ether);
_stage[5].min = 1000;
_stage[5].max = 3000;


_stage[6].bonus = 5;
_stage[6].vesting_time = 6;
_stage[6].total_tokens = 56250000 * (1 ether);
_stage[6].min = 1000;
_stage[6].max = 3000;


_stage[7].bonus = 0;
_stage[7].vesting_time = 6;
_stage[7].total_tokens = 56250000 * (1 ether);
_stage[7].min = 1000;
_stage[7].max = 3000;

_stage[8].bonus = 0;
_stage[8].vesting_time = 12;
_stage[8].total_tokens = 52000000 * (1 ether);
_stage[8].min = 1000;
_stage[8].max = 3000;
}
//struct for investing in stages

    struct stage_invest {
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
        uint total_tokens_wo_bonus;
        uint original_tokens_stage;
        uint has_payments;
    }

    mapping(uint=>mapping(address=>stage_invest)) public _stage_invest;  // _stage_invest[stage][address]
    mapping(uint=>mapping(uint => address)) public _stage_invest_index;  //for iterations _stage_invest_index(address)[stage][index]
    mapping(uint=>uint) public _stage_count;                             //count in stage _stage_count[stage][number_of_records]

    struct Admins {
      bool active;
    }

    mapping(address=>Admins) public  _admins;

   struct Owners {
     bool active;
   }

   mapping(address=>Owners) public _owners;
// DECLARATION OF VARIABLES ==================== end ===========================


//##############################################################################
//                                 MODIFIERS
//##############################################################################
modifier OnlyOwner(address _sender_address) {
  require(_owners[_sender_address].active == true);
  _;

}
modifier OnlyAdmin(address _admin_address) {
  require(_admins[_admin_address].active == true);
  _;
}

modifier StopContract(uint _stop_contract_state) {
  require(_stop_contract_state == 0);
  _;
}

modifier InvestorCheck(address _investor_address_check) {
  require(_investors[_investor_address_check].active == true);
  _;
}

modifier KycCheck(address _investor_address) {
  require(_investors[_investor_address].kyc == true);
  _;
}
//set admin active or not ====================== start =========================
function set_rate(uint _ether_rate, uint _bitcoin_rate, uint _token_cent) public OnlyOwner(msg.sender) {
  token_rate = _ether_rate;
  bitcoin_rate = _bitcoin_rate;
  token_to_cent = _token_cent;
}
//test function for setting Admins
function set_admin_initial() internal {
  /* _admins[0x22346eC7cC90Dfd971a92c731904B79e0bEc302E].active = true;
  _admins[0x38a22A715753eA01533930dF3cb112FdBcd0a9AB].active = true;
  _admins[0xaAd34d37DBb5a91d25c651080D147dF59e35A957].active = true;
  _admins[0xa14D47b43D92F4F0e2f474c2b059D0C2000318BF].active = true;
  _admins[0x54e7d4954b2e0c4BbEa6E55B5F89a6A6359b695C].active = true;
  _admins[0xf71260be31dc3c437dd90f91e7d71eaaaa4e0a8e].active = true; */

  _admins[0x7f0eb43564178c4ab1d4ecd29dde0090c83f8cef].active = true;
  _admins[0x7108b9f569ce347cd4b7028d9cc6c76b961be736].active = true;

}

function set_owners_initial() internal {
    /* _owners[0xa14D47b43D92F4F0e2f474c2b059D0C2000318BF].active = true;
    _owners[0xaAd34d37DBb5a91d25c651080D147dF59e35A957].active = true; */
    _owners[0x1d6ace2130315e86b9a1c1bba207deaff91876d5].active = true;


}

function set_standard_kyc_limit(uint _kyc, uint _kyc_extended) OnlyOwner(msg.sender) {
  dollar_limit_notext_kyc = _kyc;
  dollar_limit_ext_kyc = _kyc_extended;
}


function set_admin(address _admin_address, bool _active) public
    OnlyOwner(msg.sender)
    returns (bool)
    {
      _admins[_admin_address].active = _active;

    }

//SET INVESTOR ========================= start =================================
   function set_investor(address _address, bool _kyc) public
     OnlyAdmin(msg.sender)
     StopContract(stop_contract_state)
     //returns (string)
     {
       _investors[_address].active = true;
       _investors[_address].signup = 1;
       _investors[_address].kyc = _kyc;
  }

  function set_limits(uint _dollar_limit_notext_kyc, uint _dollar_limit_ext_kyc)
                      public
                      OnlyAdmin(msg.sender)
        {
          dollar_limit_notext_kyc = _dollar_limit_notext_kyc;
          dollar_limit_ext_kyc = _dollar_limit_ext_kyc;
        }

  function set_investor_active(address _investor_address, bool _investor_active)
                              public
                              OnlyAdmin(msg.sender)
      {
        _investors[_investor_address].active = _investor_active;
        emit SetInvestor(_investor_address,
                         _investor_active);


      }

function set_investor_ext_kyc(address _investor_address, bool _extended_kyc)
                              public
                              OnlyAdmin(msg.sender)
{
  _investors[_investor_address].extended_kyc = _extended_kyc;

  emit SetInvestorKyc(_investor_address,
                      _extended_kyc);


}
//SET INVESTOR ========================= end ===================================


//SETTING OF STAGE ============================= start =========================

    function set_current_stage(uint _current_stage,
                              uint _duration, uint _min, uint _max)
                              public
                              OnlyAdmin(msg.sender)
                              StopContract(stop_contract_state)
                              returns(uint)
    {
        require(_current_stage == stage_count+1 && _current_stage <= 8);

        _stage[_current_stage].start_date = get_current_date();

        _stage[_current_stage].duration = _duration;
        _stage[_current_stage].min = _min;
        _stage[_current_stage].max = _max;
        _stage[_current_stage].active = true;
        stage_count += 1;
        current_stage = _current_stage;
        return current_stage;
    }

//PAYMENTS ALL WAYS ========================= start ============================
//when we get ether from investors
   function ()
     payable public
     StopContract(stop_contract_state)
     InvestorCheck(msg.sender)
     KycCheck(msg.sender)
     {
     require(_investors[msg.sender].signup == 1);
     require(current_stage != 8);
     uint _tokens = msg.value*token_rate;
     //_tokens = CheckLimitDollars(msg.sender, _tokens);
     require(CheckLimitDollars(msg.sender, _tokens) == false);
     _investors[msg.sender].total_original_invest += _tokens;
     _stage_invest[current_stage][msg.sender].original_tokens_stage += _tokens;
     require(_investors[msg.sender].total_invest + _tokens <= general_limit_stage);
     //uint _investing_amount;
     require(RequireGetPayment(_tokens,msg.sender) == true);
     uint _current_stage = current_stage;
     var (_investing_amount) = AddPayment(msg.sender, _tokens );
     _investors[msg.sender].total_invest_ether += msg.value;
     emit Payment( '',
             msg.sender,
             current_date,
             msg.value,
             0,
             _investing_amount,
             _current_stage,
             'from_ether');


   }


   //when we get ether from investors
      function add_ether_to_investor(address _investors_address) public
        payable
        OnlyAdmin(msg.sender)
        StopContract(stop_contract_state)
        KycCheck(_investors_address)
        {
        require(_investors[_investors_address].signup == 1);
        require(current_stage != 8);
        uint _curent_stage = current_stage;
        uint _tokens = msg.value*token_rate;
        uint _investing_amount;
      //  _tokens =
        require(CheckLimitDollars(_investors_address, _tokens) == false);
        require(_investors[_investors_address].total_invest + _tokens <= general_limit_stage);
        //uint _investing_amount;
        require(RequireGetPayment(_tokens,_investors_address) == true);
        _investors[_investors_address].total_original_invest += _tokens;
        _stage_invest[current_stage][_investors_address].original_tokens_stage += _tokens;
        _investing_amount = AddPayment(_investors_address, _tokens );
        _investors[_investors_address].total_invest_ether += msg.value;
       emit Payment( '',
                _investors_address,
                current_date,
                msg.value,
                0,
                _investing_amount,
                _curent_stage,
                'add_ether');


      }
   //set tokens for investor

    function add_tokens_to_investor(address _address, uint _tokens_for_add) public
        OnlyAdmin(msg.sender)
        StopContract(stop_contract_state)
        {
        require(_investors[_address].signup == 1 && _investors[_address].active == true);
        require(_investors[_address].total_invest + _tokens_for_add <= general_limit_stage);
        require(RequireGetPayment(_tokens_for_add,_address) == true);
        uint _current_stage = current_stage;
        //uint _investing_amount;
        var _investing_amount = AddPayment(_address,  _tokens_for_add);
        emit Payment( '',
                 _address,
                 current_date,
                 0,
                 0,
                 _investing_amount,
                 _current_stage,
                 'add_tokens');
    }



//function add tokens from bitcoin
/* function give_tokens_bitcoin(address _investors_address,
                             uint _bitcoin_amount,
                             string _from_bitcoin_address) public
                             OnlyAdmin(msg.sender)
                             StopContract(stop_contract_state)
      {
        require(current_stage != 8);
        require(_investors[_investors_address].signup == 1 && _investors[_investors_address].active == true);
        uint _btc_tokens = (_bitcoin_amount*bitcoin_rate)/100000000;
        require(_investors[_investors_address].total_invest + _btc_tokens <= general_limit_stage);
        _btc_tokens = _btc_tokens * (1 ether);
        require(RequireGetPayment(_btc_tokens,_investors_address) == true);
        uint _current_stage = current_stage;
        //uint _investing_amount;
        var _investing_amount = AddPayment(_investors_address, _btc_tokens);

        _investors[_investors_address].total_invest_btc += _bitcoin_amount;

        emit Payment( _from_bitcoin_address,
                 _investors_address,
                 current_date,
                 0,
                 _bitcoin_amount,
                 _investing_amount,
                 _current_stage,
                 'from_btc');

    } */
//function for checking of posibility of payment at this time
function RequireGetPayment(uint _bonus, address _investors_address) internal  returns(bool) {
     require(current_stage < 9 && current_stage != 0);
     uint current_date_now;
     uint _in_dollars;
     uint _in_tokens_amount;
     current_date_now = get_current_date();

     _in_tokens_amount = _bonus/(1 ether);
     bool _difference_more;
     bool _min_max;
     bool _date_check;
     uint max_dollars = _stage[current_stage].max*100/token_to_cent;
     uint min_dollars = _stage[current_stage].min*100/token_to_cent;
     // check of difference sold_token + _bonus and sold_token

      if ( max_dollars >= _in_tokens_amount && min_dollars <= _in_tokens_amount) {
          _min_max = true;
      }
     //check date if stage exp. = stop
      if (current_date_now < (_stage[current_stage].start_date + _stage[current_stage].duration)) {
          _date_check = true;
      }

      if (_min_max == true && _date_check == true) {

          return true;
      } else
      {
        return false;
      }

  }

  //function for check and back ether if more then 17999 and no deep kyo

  function CheckLimitDollars(address _address,
                             uint _tokens) internal returns(bool)
  {
    uint tokens_for_invest;
    uint limit_dollars_in_tokens = dollar_limit_notext_kyc*100/token_to_cent*(1 ether);
    uint limit_dollars_in_tokens_kyc = dollar_limit_ext_kyc*100/token_to_cent*(1 ether);
    uint tokens_over;
    bool stop_payment;
    uint total_tokens_after = _investors[_address].total_original_invest + _tokens;
    if (_investors[_address].extended_kyc == false && total_tokens_after > limit_dollars_in_tokens ) {
        tokens_over = total_tokens_after - limit_dollars_in_tokens;
        stop_payment = true;
    } else {
    if (_investors[_address].extended_kyc == true && total_tokens_after > limit_dollars_in_tokens_kyc) {
      tokens_over = total_tokens_after - limit_dollars_in_tokens_kyc;
      stop_payment = true;
    }
    else { stop_payment = false; } }

  emit OverLimit(_address,
                   current_date,
                   tokens_over);
   return stop_payment;

  }
  //function of payemnts for all ways
      function AddPayment( address _address,
                           uint _tokens)
                           internal
                           returns(uint)
      {


          // check if this investor had payments in current stage
          if (_stage_invest[current_stage][_address].has_payments == 0)
          {
              _stage[current_stage].total_investors += 1;  //increment of total investors in stage
              _stage_invest_index[current_stage][_stage[current_stage].total_investors] = _address;
              _stage_invest[current_stage][_address].has_payments = 1;  //save event of payment for next time
          }
          _stage[current_stage].sold_token += _tokens;
          _stage_invest[current_stage][_address].total_tokens_wo_bonus += _tokens;

          //calculate tokens with bonuses (bonus in persent)
          uint bonus = _stage[current_stage].bonus;
          uint investing_amount = _tokens + _tokens/100*bonus;
          _stage_invest[current_stage][_address].locked_amount += investing_amount;
          _stage_invest[current_stage][_address].total_invest_stage += investing_amount;
          _investors[_address].total_invest += investing_amount;
          _investors[_address].locked_amount += investing_amount;

           if (_stage[current_stage].sold_token >= _stage[current_stage].total_tokens) {
              _stage[current_stage].active = false;
              if (current_stage == 7) {
                  ico_end = true;
                  end_ico_year = getYear(current_date);
                  end_ico_month = getMonth(current_date);
                  end_ico_day = getDay(current_date);
                  }
              current_stage = 0;
          }
           return investing_amount;

      }
//PAYMENTS ALL WAYS ========================= end ++============================
//stop contract

function stop_contract(uint _stop_contract) OnlyOwner(msg.sender) {
  stop_contract_state = _stop_contract;
}
//CRON START ================================ start ============================
 function get_month_from_ico_end() public view returns(uint months_from_end_ico) {
     if (getYear(current_date) == end_ico_year)
     {
     months_from_end_ico = getMonth(current_date) - end_ico_month;
     }
     else
     {
     months_from_end_ico = getMonth(current_date)+(getYear(current_date)-(end_ico_year+1))*12 + (12-end_ico_month);
     }
  }

function Calculate() public OnlyAdmin(msg.sender) returns(uint stage_now, bool recalculate) {
    if (current_stage != 0) {
       if ((current_date - _stage[current_stage].start_date) >= _stage[current_stage].duration) {
            if (current_stage == 7 && ico_end != true) {
                ico_end = true;
                end_ico_year = getYear(current_date);
                end_ico_month = getMonth(current_date);
                end_ico_day = getDay(current_date);

            }
            _stage[current_stage].active = false;
            current_stage = 0;
        }
    }

recalculate = false;
if (ico_end == true) {
  bool _can_go;
  uint months_from_ico_end = get_month_from_ico_end();
  if ((months_from_ico_end - vesting_date_count) == 1 && getDay(current_date) >= end_ico_day) {
    _can_go = true;
  }
  if ((months_from_ico_end - vesting_date_count) > 1) {
    _can_go = true;
  }
    bool plus_persent;
    uint stage_in;
  if (months_from_ico_end != vesting_date_count && _can_go == true) {
    vesting_date_count += 1;
    if ((vesting_date_count - bonus_count) % 2 == 0)
    {
      plus_persent = true;
      bonus_count = vesting_date_count;
    } else { plus_persent = false; }


  //not calculate when it was ran
    uint stage_for_calculation;

//stage cicle
    for(uint s=1; s<9; s++){
        uint vesting_time_in_stage = _stage[s].vesting_time;


if (_stage[s].total_investors != 0) {
        for (uint j=1; j<=_stage[s].total_investors; j++) {

       if ( months_from_ico_end >= vesting_time_in_stage && _stage_invest[s][_stage_invest_index[s][j]].locked_amount > 0)
         {
           _stage_invest[s][_stage_invest_index[s][j]].unlocked_amount += _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;
           _stage_invest[s][_stage_invest_index[s][j]].locked_amount -= _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;
           _investors[_stage_invest_index[s][j]].unlocked_amount += _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;
           _investors[_stage_invest_index[s][j]].locked_amount -= _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;

         }

         if (plus_persent == true) {
          if((_stage_invest[s][_stage_invest_index[s][j]].unlocked_amount + _stage_invest[s][_stage_invest_index[s][j]].locked_amount) > 0)
             {
             uint dividents = (_stage_invest[s][_stage_invest_index[s][j]].unlocked_amount + _stage_invest[s][_stage_invest_index[s][j]].locked_amount)/100;
             _stage_invest[s][_stage_invest_index[s][j]].dividends += dividents;
             _stage_invest[s][_stage_invest_index[s][j]].unlocked_amount += dividents;
             _investors[_stage_invest_index[s][j]].unlocked_amount += dividents;
             }
          }
      }
}
//list of investors from stage
   }
//stage cicle
  }
}

    stage_now = current_stage;
}


function tokens_withdraw(uint _tokens_amount) public returns(string) {
  require(_investors[msg.sender].unlocked_amount >= _tokens_amount);
  MONIToken(token_address).transfer(msg.sender,_tokens_amount);
  uint withdraw_date = get_current_date();
  _investors[msg.sender].unlocked_amount -= _tokens_amount;
  _investors[msg.sender].total_withdraw += _tokens_amount;
  uint _tokens_amount_event = _tokens_amount;
  bool stop;
  for (uint i = 1;i<9;i++) {
    if (_stage_invest[i][msg.sender].unlocked_amount != 0 && _tokens_amount != 0) {
      if (_stage_invest[i][msg.sender].unlocked_amount >= _tokens_amount) {
      _stage_invest[i][msg.sender].unlocked_amount -= _tokens_amount;
      _tokens_amount = 0;
      }
      else {
        _tokens_amount = _tokens_amount - _stage_invest[i][msg.sender].unlocked_amount;
        _stage_invest[i][msg.sender].unlocked_amount = 0;
      }
    }
  }

  emit Payment( '',
           msg.sender,
           current_date,
           0,
           0,
           _tokens_amount_event,
           0,
           'withdraw');
  return "Withdraw OK";
}

function tokens_withdraw_admin(address _to,uint _tokens_amount)
  public
  OnlyAdmin(msg.sender)
  {
    require(_investors[_to].unlocked_amount >= _tokens_amount);
    if (_investors[_to].unlocked_amount-_tokens_amount < 1*(1 ether)) {
      _tokens_amount = _investors[_to].unlocked_amount;
    }
    MONIToken(token_address).transfer(_to,_tokens_amount);
    uint withdraw_date = get_current_date();
    _investors[_to].unlocked_amount -= _tokens_amount;
    _investors[_to].total_withdraw += _tokens_amount;
    uint _tokens_amount_event = _tokens_amount;
    for (uint i = 1;i<9;i++) {
      if (_stage_invest[i][_to].unlocked_amount != 0 && _tokens_amount != 0) {
        if (_stage_invest[i][_to].unlocked_amount >= _tokens_amount) {
        _stage_invest[i][_to].unlocked_amount -= _tokens_amount;
        _tokens_amount = 0;
        }
        else {
          _tokens_amount = _tokens_amount - _stage_invest[i][msg.sender].unlocked_amount;
          _stage_invest[i][_to].unlocked_amount = 0;
        }
      }
    }

    emit Payment( '',
             _to,
             current_date,
             0,
             0,
             _tokens_amount_event,
             0,
             'withdraw_a');
  }

function withdraw_ether(uint _ether) public OnlyOwner(msg.sender) {
  require(ico_end == true);
  require(address(this).balance >= _ether);
  msg.sender.transfer(_ether);
}

function refund_investor(address _investors_address)  public OnlyAdmin(msg.sender) {
  require(ico_end == true);
  uint _total_invest_ether =  _investors[_investors_address].total_invest_ether;
  if (_total_invest_ether != 0 ) {
        _investors_address.transfer(_investors[_investors_address].total_invest_ether);
        _investors[_investors_address].total_invest_ether = 0;
      }

      emit Payment( '',
               _investors_address,
               current_date,
               _total_invest_ether,
               0,
               0,
               0,
               'refund_investor');
    }

function refund_all()  public  OnlyOwner(msg.sender) returns (bool) {
  for(uint s=1; s<9; s++)
  {
    for (uint j=1; j<=_stage[s].total_investors; j++)
    {
        if(_investors[_stage_invest_index[s][j]].total_invest_ether != 0 )
        {
          address investor = _stage_invest_index[s][j];

investor.transfer(_investors[_stage_invest_index[s][j]].total_invest_ether);

emit Payment( '',
         _stage_invest_index[s][j],
         current_date,
         _investors[_stage_invest_index[s][j]].total_invest_ether,
         0,
         0,
         0,
         'refund_investor');

_investors[_stage_invest_index[s][j]].total_invest_ether = 0;

        }
    }
  }
}


function SetCurrentDate(uint _current_date) public returns(uint){
        current_date = _current_date;
        return current_date;
    }

}
