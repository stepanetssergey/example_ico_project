var MoniContract = artifacts.require("./MoniContract.sol");

contract('MoniContract', (accounts) => {
  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];

//Test of creation of Investors
  it("Set addmin for testing", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_admin_initial(firstAdmin,{from:creatorAddress});
    })
  });

  it("Call add investors must be passed!", () => {
    MoniContractInstance = MoniContract.deployed();
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor.call(firstInvestor,{from:firstAdmin});
    })
    .then(result => {
      assert.equal(result,'Singup OK','Call function of creating have some error');
    });
  });

  it("Add investor return opcode 0x1", () => {
    MoniContractInstance = MoniContract.deployed();
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor(firstInvestor,{from:firstAdmin});
    })
    .then(result => {
      assert.equal(result['receipt']['status'],'0x1','Investor is not created');
    });
  });

  it("Try to write investor again and function must return Singed up allready", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor.call(firstInvestor,{from:firstAdmin});
    })
    .then(result => {
      assert.equal(result,"Singed up allready",'Investor have been written again');
    });
  });

  it("Try to creating of investor should revert transaction if it will be from not admin address", () =>{
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor(firstInvestor,{from:secondInvestor});
    })
    .then(result => {
      assert.fail();
    })
    .catch(error => {
      assert.notEqual(error.message,'assert.fail()','It is possible to access to function for writing of investors from not admin address');
    });
  })





});
