var MoniContract = artifacts.require("./MoniContract.sol");

contract('MoniContract', (accounts) => {
  var ETHER_FOR_INVEST;
  var GET_TOKENS_AMOUNT;
  var DURATION = 12000;
  var MAX = 60;
  var MIN = 12;
  var MAX_TOKENS_INVEST = 60000000000000000000;

  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];

//Test of creation of Investors
  it("Set addmin for testing", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_admin_initial(firstAdmin,{from:creatorAddress});
    })
  });

  it("Set stage 1 with values", () => {
    MoniContractInstance = MoniContract.deployed();
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage.call(1,DURATION ,MIN,MAX, MAX_TOKENS_INVEST,{from:firstAdmin});
    })
    .then(result => {
      assert.equal(result,1,'Set stage to 1 doesn\'t work');
    });
  });

  it("It should revert transaction if try to create stage1 from not admin address", () =>{
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage.call(1,DURATION,MIN,MAX, MAX_TOKENS_INVEST,{from:secondInvestor});
    })
    .then(result => {
      assert.fail();
    })
    .catch(error => {
      assert.notEqual(error.message,'assert.fail()','It is possible to create stage1 from not admin address');
    });
  });

  var date = new Date();
  var set_date = Math.round(date.getTime()/1000);
  console.log('Data of setting stage 1:',set_date);
  it("Set stage 1", () => {
    MoniContractInstance = MoniContract.deployed();
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage(1,DURATION ,MIN,MAX, MAX_TOKENS_INVEST,{from:firstAdmin});
    })
  });
  var Datetime_for_test = 'Set datetime for test to '+set_date+' pluss DURATION: '+DURATION+' as result -- '+set_date+DURATION;
  it(Datetime_for_test, () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.SetCurrentDate(set_date+DURATION,{from:firstAdmin});
    })
  });
  it("Get setting date", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.SetCurrentDate.call(set_date+DURATION,{from:firstAdmin});
    })
    .then(result=>{
      assert.equal(result.toNumber(),set_date+DURATION,'Setting date is wrong !!')
    })
  });


  it("Call calculate like from crone", () => {
    return MoniContract.deployed()
    .then(instance =>{
      return instance.Calculate.call({from:firstAdmin});
    })
    .then(result =>{
      assert.equal(result[0].toNumber(),0,"Cron doesn\'t stop stage when start date + duration = current date !!!");
    })
  });

});
