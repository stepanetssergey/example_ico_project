var MoniContract = artifacts.require("./MoniContract.sol");

contract('MoniContract', (accounts) => {
  var ETHER_FOR_INVEST;
  var GET_TOKENS_AMOUNT;
  var DURATION = 12000;
  var MAX = 60;
  var MIN = 12;
  var MAX_TOKENS_INVEST = web3.toWei(600,"ether");
  var AMOUNT_IN_ETHER = 0.09; //12$
  var AMOUNT_IN_ETHER_MIN = 0.01
  var AMOUNT_IN_ETHER_MAX = 0.11
  var TOKEN_FOR_INVESTOR = web3.toWei(300,"ether");
  var TOKEN_FOR_INVESTOR_OVER = web3.toWei(601,"ether");
  var STAGE = 1;
  var BONUS_IN_PERSENT = 50;

  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];

//Test of creation of Investors
  it("Set addmin for testing", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_admin_initial(firstAdmin,{from:creatorAddress});
    })
  });

  it("Investor registration", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor(firstInvestor,{from:firstAdmin})
    })
  });

  it("Set stage 1", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage(1,DURATION ,MIN,MAX, MAX_TOKENS_INVEST,{from:firstAdmin});
    })
  });

  it("Get investor check", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._investors.call(firstInvestor);
    assert.equal(result[3],true,"Investor not active!")
  });

  it("Send tokens to investor NORMAL AMOUNT", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.add_tokens_to_investor(firstInvestor,TOKEN_FOR_INVESTOR,{from:firstAdmin});
    })
    .then(result => {
    assert.fail();
    })
    .catch(error => {
      assert.equal(error.message,'assert.fail()',"Payments is not possible !")
    });
  });

  var text_for_test_sold_token = "Should return amount of tokens = "+TOKEN_FOR_INVESTOR;
  it(text_for_test_sold_token, async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._stage.call(STAGE);
    assert.equal(result[4].toNumber(),TOKEN_FOR_INVESTOR,'sold token is wrong !');
  });

  var expected_tokens = parseInt(TOKEN_FOR_INVESTOR)+(parseInt(TOKEN_FOR_INVESTOR)/100*BONUS_IN_PERSENT);
  var text_for_locked_amount = "Should return locked amount of tokens = "+expected_tokens;
  it(text_for_locked_amount, async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._investors.call(firstInvestor);
    assert.equal(result[4].toNumber(),expected_tokens,'Locked amount in stage is wrong !');
  });
  var text_for_locked_amount = "Should return total invest amount of tokens = "+expected_tokens;
  it(text_for_locked_amount, async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._investors.call(firstInvestor);
    assert.equal(result[0].toNumber(),expected_tokens,'Total invest amount in stage is wrong !');
  });

  // it("Send tokens to investor MORE OVER LIMIT", () => {
  //   return MoniContract.deployed()
  //   .then(instance => {
  //     return instance.add_tokens_to_investor(firstInvestor,TOKEN_FOR_INVESTOR_OVER,{from:firstAdmin});
  //   })
  //   .then(result => {
  //   assert.fail();
  //   })
  //   .catch(error => {
  //     assert.notEqual(error.message,'assert.fail()',"Payments is possible over tokens limit !")
  //   });
  // });

});
