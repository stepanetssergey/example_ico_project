var MoniContract = artifacts.require("./MoniContract.sol");

contract('MoniContract', (accounts) => {
  var ETHER_FOR_INVEST;
  var GET_TOKENS_AMOUNT;
  var DURATION = 12000;
  var MAX = 60;
  var MIN = 12;
  var MAX_TOKENS_INVEST = web3.toWei(600,"ether");
  var AMOUNT_IN_ETHER = 0.09; //12$
  var AMOUNT_IN_ETHER_MIN = 0.01
  var AMOUNT_IN_ETHER_MAX = 0.11

  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];

//Test of creation of Investors
  it("Set addmin for testing", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_admin_initial(firstAdmin,{from:creatorAddress});
    })
  });

  it("Investor registration", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor(firstInvestor,{from:firstAdmin})
    })
  });

  it("Set stage 1", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage(1,DURATION ,MIN,MAX, MAX_TOKENS_INVEST,{from:firstAdmin});
    })
  });

  it("Get investor check", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._investors.call(firstInvestor);
    //console.log(result);
  })

  it("Send Ether to contract NORMAL AMOUNT", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.sendTransaction({from:firstInvestor, to:MoniContractInstance.address, value: web3.toWei(AMOUNT_IN_ETHER,"ether")});
    })
    .then(result => {
    assert.fail();
    })
    .catch(error => {
      assert.equal(error.message,'assert.fail()',"Payments is not possible !")
    });
  });

    it("Send Ether to contract MORE THEN MAX", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.sendTransaction({from:firstInvestor, to:MoniContractInstance.address, value: web3.toWei(AMOUNT_IN_ETHER_MAX,"ether")});
      })
      .then(result => {
      assert.fail();
      })
      .catch(error => {
        assert.notEqual(error.message,'assert.fail()',"Payments is possible and max amount in dollars is ignored !")
      });
    });

    it("Send Ether to contract LESS THEN MIN", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.sendTransaction({from:firstInvestor, to:MoniContractInstance.address, value: web3.toWei(AMOUNT_IN_ETHER_MIN,"ether")});
      })
      .then(result => {
      assert.fail();
      })
      .catch(error => {
        assert.notEqual(error.message,'assert.fail()',"Payments is possible and min amount in dollars is ignored !")
      });
    });
    it("Send Ether to contract NORMAL AMOUNT one more time", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.sendTransaction({from:firstInvestor, to:MoniContractInstance.address, value: web3.toWei(AMOUNT_IN_ETHER,"ether")});
      })
      .then(result => {
      assert.fail();
      })
      .catch(error => {
        assert.equal(error.message,'assert.fail()',"Payments is not possible !")
      });
    });

    it("Must close stage1", async () =>{
      let moni = await MoniContract.deployed();
      let result = await moni.current_stage.call();
      assert.equal(result.toNumber(),0,'Payment doesn\'t close stage 1');
    })
});
