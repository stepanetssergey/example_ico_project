var MoniContract = artifacts.require("./MoniContract.sol");

//for (i=1;i<7;i++) {
//  console.log('TEST FOR STAGE:',i);
contract('MoniContract', (accounts) => {
  var STAGE = 1;
  var ETHER_FOR_INVEST;
  var GET_TOKENS_AMOUNT;
  var DURATION = 12000;
  var MAX = 60;
  var MIN = 12;
  var MAX_TOKENS_INVEST = web3.toWei(600,"ether");
  var AMOUNT_IN_ETHER = 0.09; //12$
  var AMOUNT_IN_ETHER_MIN = 0.01
  var AMOUNT_IN_ETHER_MAX = 0.11
  var TOKEN_FOR_INVESTOR = web3.toWei(300,"ether");
  var TOKEN_FOR_INVESTOR_OVER = web3.toWei(601,"ether");
  var BONUS_IN_PERSENT = 50;
  var BITCOIN_AMOUNT = (0.0018 * 100000000); //in btc
  var BITCOIN_AMOUNT_MORE = (0.08 * 100000000); //in btc
  var TOKENS_FROM_BITCOIN = web3.toWei((BITCOIN_AMOUNT*4*100000/100000000),"ether");

  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];

//Test of creation of Investors
  it("Set addmin for testing", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_admin_initial(firstAdmin,{from:creatorAddress});
    })
  });

  it("Investor registration", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor(firstInvestor,{from:firstAdmin})
    })
  });

  it("Set stage 1", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage(STAGE,DURATION ,MIN,MAX, MAX_TOKENS_INVEST,{from:firstAdmin});
    })
  });

  it("Get investor check", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._investors.call(firstInvestor);
    assert.equal(result[3],true,"Investor not active!")
  });
  it("Get stage check before payment", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni.current_stage.call();
    assert.notEqual(result.toNumber(),0,"Stage is invalid!")
  });

  var text_btc_function = "Send btc in satoshi = "+BITCOIN_AMOUNT;
  it(text_btc_function, () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.give_tokens_bitcoin(firstInvestor,BITCOIN_AMOUNT,"0sdd878987283478498737829",{from:firstAdmin});
    })
    .then(result => {
    assert.fail();
    })
    .catch(error => {
      assert.equal(error.message,'assert.fail()',"Payments is not possible !")
    });
  });

  var text_btc_function = "Send btc in satoshi = "+BITCOIN_AMOUNT_MORE + " in this case it should revert transaction";
  it(text_btc_function, () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.give_tokens_bitcoin(firstInvestor,BITCOIN_AMOUNT_MORE,"0sdd878987283478498737829",{from:firstAdmin});
    })
    .then(result => {
    assert.fail();
    })
    .catch(error => {
      assert.notEqual(error.message,'assert.fail()',"Payments is  possible limits don\'t work!")
    });
  });

  var text_btc_function_2 = "Send btc in satoshi = "+BITCOIN_AMOUNT*3+"and then check status";
  it(text_btc_function_2, () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.give_tokens_bitcoin(firstInvestor,BITCOIN_AMOUNT*3,"0sdd878987283478498737829",{from:firstAdmin});
    })
    .then(result => {
    assert.fail();
    })
    .catch(error => {
      assert.equal(error.message,'assert.fail()',"Payments is not possible !")
    });
  });

  var tokens_with_bonus = parseInt(TOKENS_FROM_BITCOIN)+parseInt(TOKENS_FROM_BITCOIN)/100*BONUS_IN_PERSENT;
  it("Should return amount of locked tokens", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._investors.call(firstInvestor);
    console.log(result[4].toNumber());
    console.log(tokens_with_bonus);
    assert.equal(result[4].toNumber(),tokens_with_bonus,"Locked amount is wrong!")
  });

  var text_for_test_sold_token = "Should return amount of tokens = "+TOKENS_FROM_BITCOIN;
  it(text_for_test_sold_token, async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni._stage.call(STAGE);
    console.log(result[4].toNumber());
    assert.equal(result[4].toNumber(),TOKENS_FROM_BITCOIN,'sold token is wrong !');
  });


  it("Get stage check", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni.current_stage.call();
    console.log(result.toNumber());
    assert.equal(result.toNumber(),0,"Stage is invalid!")
  });

});
//}
