var MoniContract = artifacts.require("./MoniContract.sol");
var Token = artifacts.require("./MONIToken.sol");
const crypto = require('crypto');
var ethUtils = require('ethereumjs-util');

var getRandomWallet = function() {
  var randbytes = crypto.randomBytes(32);
  var address = '0x' + ethUtils.privateToAddress(randbytes).toString('hex');
  return { address: address, privKey: randbytes.toString('hex') }
}
console.log(getRandomWallet()['address']);

contract('MoniContract', (accounts) => {


  var DURATION = 1000;
  var MIN = 100;
  var MAX = 150000;
  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];
  var thirdInvestor = accounts[5];
  var notlistInvestor = accounts[10];

  var stage1_tokens = web3.toWei(1500000,"ether");
  var stage2_tokens = web3.toWei(1000000,"ether");
  var stage3_tokens = web3.toWei(500000,"ether");
  var stage4_tokens = web3.toWei(12000,"ether");
  var stage5_tokens = web3.toWei(12000,"ether");
  var stage6_tokens = web3.toWei(12000,"ether");
  var stage7_tokens = web3.toWei(12000,"ether");


  var current_timestamp = Math.round(new Date().getTime()/1000);
  it("Set date - current_date (initial date)", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.SetCurrentDate(current_timestamp,{from:firstAdmin});
    })
    .then(result =>{
      //console.log(result);
      console.log(Math.round(new Date.today().getTime()/1000));
    });
  });


    it("Mine tokens for ICO", () =>{
      return Token.deployed()
      .then(token => {
        return token.mine(web3.toWei(10000000000,"ether"),{from:creatorAddress});
      });
    });

    it("Check balance of tokens", () => {
      return Token.deployed()
      .then(token => {
        return token.balanceOf(creatorAddress);
      })
      .then(balance =>{
        assert.equal(balance.toNumber(),web3.toWei(10000000000,"ether"),"Not equal supp. token amount");
        console.log(balance.toNumber());
      })

    });

    it("Tranfser tokens to contract", () => {
      return Token.deployed()
      .then(token => {
      return token.transfer(MoniContract.address,
                            web3.toWei(100000,"ether"),
                            {from:creatorAddress});
      })
      .then(result => {
        assert.fail();
      })
      .catch(error => {
  //      console.log(error);
        assert.equal(error.message,'assert.fail()',"Transfer tokens is not possible !");
      })

    });


    // it("Set addmin for testing", () => {
    //   return MoniContract.deployed()
    //   .then(instance => {
    //     return instance.set_admin(firstAdmin,{from:creatorAddress});
    //   })
    // });
    it("Get admin First", async () =>{
      let moni = await MoniContract.deployed();
      let result = await moni._admins.call(firstAdmin,{from:firstAdmin});
      console.log("Current admin first:",result);
    });

    it("Investor registration first", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.set_investor(firstInvestor,false,{from:firstAdmin})
      })
    });

for (var i = 1;i < 5; i++) {
    it("Set investor fake -"+i, () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.set_investor(getRandomWallet()['address'],false,{from:firstAdmin})
      })
    });
}
//Set limit for stages
    it("Set limit for stages", () => {
      return MoniContract.deployed()
      .then(instance =>{
        return instance.set_general_limit(10000000,{from:firstAdmin})
      })
    });

// Set stage 1 and transfer tokens to first investor



  it("Set stage 1", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_current_stage(1,DURATION,MIN,MAX,{from:firstAdmin});
    })
    .then(result => {
      assert.fail();
    })
    .catch(error => {
      assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
    })
  });

  it("Set tokens for stage1", () =>{
    return MoniContract.deployed()
    .then(instance => {
      return instance.add_tokens_to_investor(firstInvestor,stage1_tokens,{from:firstAdmin});
    })
    .then(result =>{
      assert.fail();
    })
    .catch(error => {
      assert.equal(error.message,'assert.fail()',"Set tokens for investor 1 stage 1")
    });
  });

  it("Get unlocked_amount from first Investor stage 1", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance._investors.call(firstInvestor,{from:firstAdmin});
    })
    .then(result => {
      console.log("Unlocked amount",result[5].toNumber());
      console.log("Locked amount",result[4].toNumber());
    })
  });

  it("Set date = current_date + DURATION", async () =>{
    let moni = await MoniContract.deployed();
    let result = await moni.current_date.call();
    console.log("Current date befor:",result.toNumber());
    await moni.SetCurrentDate(result.toNumber()+DURATION);
    await moni.Calculate({from:firstAdmin});
    let result_after = await moni.current_date.call();
    console.log("Current date after:",result_after.toNumber());
  });

  it("Run Calculate next month", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.Calculate({from:firstAdmin});
    })
  });

  it("Check stop of stage1", () =>{
    return MoniContract.deployed()
    .then(instance => {
      return instance.current_stage.call();
    })
    .then(result => {
      assert.equal(result.toNumber(),0,"Stage 1 is stoped !");
    });
  });

  // Set stage 2 and transfer tokens to first investor



    it("Set stage 2", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.set_current_stage(2,DURATION,MIN,MAX,{from:firstAdmin});
      })
      .then(result => {
        assert.fail();
      })
      .catch(error => {
        assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
      })
    });

    // it("Set tokens for stage  2", () =>{
    //   return MoniContract.deployed()
    //   .then(instance => {
    //     return instance.add_tokens_to_investor(firstInvestor,stage2_tokens,{from:firstAdmin});
    //   })
    //   .then(result =>{
    //     assert.fail();
    //   })
    //   .catch(error => {
    //     assert.equal(error.message,'assert.fail()',"Set tokens for investor stage1")
    //   });
    // });

    it("Get unlocked_amount from first Investor stage 2", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance._investors.call(firstInvestor,{from:firstAdmin});
      })
      .then(result => {
        console.log("Unlocked amount",result[5].toNumber());
        console.log("Locked amount",result[4].toNumber());
      })
    });

    it("Set date = current_date + DURATION", async () =>{
      let moni = await MoniContract.deployed();
      let result = await moni.current_date.call();
      console.log("Current date befor:",result.toNumber());
      await moni.SetCurrentDate(result.toNumber()+DURATION);
      await moni.Calculate({from:firstAdmin});
      let result_after = await moni.current_date.call();
      console.log("Current date after:",result_after.toNumber());
    });

    it("Run Calculate next month", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.Calculate({from:firstAdmin});
      })
    });

    it("Check stop of stage 2", () =>{
      return MoniContract.deployed()
      .then(instance => {
        return instance.current_stage.call();
      })
      .then(result => {
        assert.equal(result.toNumber(),0,"Stage 2 is stoped !");
      });
    });

// Set stage 3 and transfer tokens to first investor



      it("Set stage 3", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.set_current_stage(3,DURATION,MIN,MAX,{from:firstAdmin});
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
        })
      });

      // it("Set tokens for stage 3", () =>{
      //   return MoniContract.deployed()
      //   .then(instance => {
      //     return instance.add_tokens_to_investor(firstInvestor,stage3_tokens,{from:firstAdmin});
      //   })
      //   .then(result =>{
      //     assert.fail();
      //   })
      //   .catch(error => {
      //     assert.equal(error.message,'assert.fail()',"Set tokens for investor stage1")
      //   });
      // });

      it("Get unlocked_amount from first Investor stage 3", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance._investors.call(firstInvestor,{from:firstAdmin});
        })
        .then(result => {
          console.log("Unlocked amount",result[5].toNumber());
          console.log("Locked amount",result[4].toNumber());
        })
      });

      it("Set date = current_date + DURATION", async () =>{
        let moni = await MoniContract.deployed();
        let result = await moni.current_date.call();
        console.log("Current date befor:",result.toNumber());
        await moni.SetCurrentDate(result.toNumber()+DURATION);
        await moni.Calculate({from:firstAdmin});
        let result_after = await moni.current_date.call();
        console.log("Current date after:",result_after.toNumber());
      });

      it("Run Calculate", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.Calculate({from:firstAdmin});
        })
      });

      it("Check stop of stage 3", () =>{
        return MoniContract.deployed()
        .then(instance => {
          return instance.current_stage.call();
        })
        .then(result => {
          assert.equal(result.toNumber(),0,"Stage 3 is stoped !");
        });
      });

// Set stage 4 and transfer tokens to first investor
      it("Set stage 4", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.set_current_stage(4,DURATION,MIN,MAX,{from:firstAdmin});
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
        })
      });

      it("Set date = current_date + DURATION", async () =>{
        let moni = await MoniContract.deployed();
        let result = await moni.current_date.call();
        console.log("Current date befor:",result.toNumber());
        await moni.SetCurrentDate(result.toNumber()+DURATION);
        await moni.Calculate({from:firstAdmin});
        let result_after = await moni.current_date.call();
        console.log("Current date after:",result_after.toNumber());
      });

      it("Run Calculate", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.Calculate({from:firstAdmin});
        })
      });

      it("Get unlocked_amount from first Investor stage 4", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance._investors.call(firstInvestor,{from:firstAdmin});
        })
        .then(result => {
          console.log("Unlocked amount",result[5].toNumber());
          console.log("Locked amount",result[4].toNumber());
        })
      });


      it("Check stop of stage 4", () =>{
        return MoniContract.deployed()
        .then(instance => {
          return instance.current_stage.call();
        })
        .then(result => {
          assert.equal(result.toNumber(),0,"Stage 4 is stoped !");
        });
      });

// Set stage 4 and transfer tokens to first investor
      it("Set stage 5", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.set_current_stage(5,DURATION,MIN,MAX,{from:firstAdmin});
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
        })
      });

      it("Set date = current_date + DURATION", async () =>{
        let moni = await MoniContract.deployed();
        let result = await moni.current_date.call();
        console.log("Current date befor:",result.toNumber());
        await moni.SetCurrentDate(result.toNumber()+DURATION);
        await moni.Calculate({from:firstAdmin});
        let result_after = await moni.current_date.call();
        console.log("Current date after:",result_after.toNumber());
      });

      it("Run Calculate", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.Calculate({from:firstAdmin});
        })
      });

      it("Get unlocked_amount from first Investor stage 5", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance._investors.call(firstInvestor,{from:firstAdmin});
        })
        .then(result => {
          console.log("Unlocked amount",result[5].toNumber());
          console.log("Locked amount",result[4].toNumber());
        })
      });


      it("Check stop of stage 5", () =>{
        return MoniContract.deployed()
        .then(instance => {
          return instance.current_stage.call();
        })
        .then(result => {
          assert.equal(result.toNumber(),0,"Stage 5 is stoped !");
        });
      });


// Set stage 6 and transfer tokens to first investor
      it("Set stage 6", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.set_current_stage(6,DURATION,MIN,MAX,{from:firstAdmin});
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
        })
      });

      it("Set date = current_date + DURATION", async () =>{
        let moni = await MoniContract.deployed();
        let result = await moni.current_date.call();
    //    console.log("Current date befor:",result.toNumber());
        await moni.SetCurrentDate(result.toNumber()+DURATION);
        await moni.Calculate({from:firstAdmin});
        let result_after = await moni.current_date.call();
      //  console.log("Current date after:",result_after.toNumber());
      });

      it("Run Calculate", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.Calculate({from:firstAdmin});
        })
      });

      it("Get unlocked_amount from first Investor stage 6", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance._investors.call(firstInvestor,{from:firstAdmin});
        })
        .then(result => {
          console.log("Unlocked amount",result[5].toNumber());
          console.log("Locked amount",result[4].toNumber());
        })
      });


      it("Check stop of stage 6", () =>{
        return MoniContract.deployed()
        .then(instance => {
          return instance.current_stage.call();
        })
        .then(result => {
          assert.equal(result.toNumber(),0,"Stage 6 is stoped !");
        });
      });
      it("Check ico end", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.ico_end.call();
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"set check ico end not right");
        })
      });



// Set stage 7 and transfer tokens to first investor
      it("Set stage 7", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.set_current_stage(7,DURATION,MIN,MAX,{from:firstAdmin});
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
        })
      });

      it("Set date = current_date + DURATION", async () =>{
        let moni = await MoniContract.deployed();
        let result = await moni.current_date.call();
        console.log("Current date befor:",result.toNumber());
        await moni.SetCurrentDate(result.toNumber()+DURATION);
        await moni.Calculate({from:firstAdmin});
        let result_after = await moni.current_date.call();
        console.log("Current date after:",result_after.toNumber());
      });

      it("Run Calculate", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.Calculate({from:firstAdmin});
        })
      });

      it("Check stop of stage 7", () =>{
        return MoniContract.deployed()
        .then(instance => {
          return instance.current_stage.call();
        })
        .then(result => {
          assert.equal(result.toNumber(),0,"Stage 7 is stoped !");
        });
      });

      it("Check ico end", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.ico_end.call();
        })
        .then(result => {
          assert.equal(result,true,"ICO is not stoped !")
        })
      })

// Set stage 8 and transfer tokens to first investor
      it("Set stage 8", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.set_current_stage(8,DURATION,MIN,MAX,{from:firstAdmin});
        })
        .then(result => {
          assert.fail();
        })
        .catch(error => {
          assert.equal(error.message,'assert.fail()',"Set stage possible from admin");
        })
      });

      it("Set date = current_date + DURATION", async () =>{
        let moni = await MoniContract.deployed();
        let result = await moni.current_date.call();
        console.log("Current date befor:",result.toNumber());
        await moni.SetCurrentDate(result.toNumber()+DURATION);
        await moni.Calculate({from:firstAdmin});
        let result_after = await moni.current_date.call();
        console.log("Current date after:",result_after.toNumber());
      });

      it("Run Calculate", () => {
        return MoniContract.deployed()
        .then(instance => {
          return instance.Calculate({from:firstAdmin});
        })
      });

      it("Check stop of stage 8", () =>{
        return MoniContract.deployed()
        .then(instance => {
          return instance.current_stage.call();
        })
        .then(result => {
          assert.equal(result.toNumber(),0,"Stage 8 is stoped !");
        });
      });

// Run every month and get unlocked_amount locked_amount and dividends for every months
for (var i=1;i<16;i++) {
    it("Set month more ", async () =>{
      let moni = await MoniContract.deployed();
      let result_year = await moni.end_ico_year.call();
      let result_month = await moni.end_ico_month.call();
      let result_day = await moni.end_ico_day.call();
      //Get prev. index = month
      let result_month_last = await moni.vesting_date_count.call();

      if(result_day < 10) {
        result_day = '0'+result_day;
      }
      if(result_month < 10) {
        result_month = '0'+result_month;
      }
      let date_result = result_year+'/'+result_month+'/'+result_day+' 12:00:00';
  //    console.log('End of ICO date',date_result);
  //    console.log(Math.round(new Date(date_result).getTime()/1000));
      var date_next = new Date(date_result).add(result_month_last.toNumber()+1).month();

  //    console.log("Next date:",date_next);
      var date_next_timestamp = Math.round(new Date(date_next).getTime()/1000);
      await moni.SetCurrentDate(date_next_timestamp,{from:firstAdmin});
      let result_current_date = await moni.current_date.call();
  //    console.log('Read date from contract:',result_current_date.toNumber());
      assert.equal(result_current_date.toNumber(),date_next_timestamp,'current date not changed');
    });



    it("Get vesting date index", async () =>{
      let moni = await MoniContract.deployed();
      let result = await moni.vesting_date_count.call();
      console.log("Get vesting index before calculate:",result.toNumber());
      //assert.equal(result.toNumber(),0,'Payment doesn\'t close stage 3');

    });

    it("Run Calculate gas estimation", async () => {
      let moni = await MoniContract.deployed();
      let result = await moni.Calculate.estimateGas({from:firstAdmin});
      console.log("Gas estimation for Calculate function", result);
    });

    it("Run Calculate next month", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance.Calculate({from:firstAdmin});
      })
    });

    it("Get vesting date index", async () =>{
      let moni = await MoniContract.deployed();
      let result = await moni.vesting_date_count.call();
      console.log("Get vesting index:",result.toNumber());
      //assert.equal(result.toNumber(),0,'Payment doesn\'t close stage 3');
    });

    // it("Get stage in recalculation", async () =>{
    //   let moni = await MoniContract.deployed();
    //   let result = await moni.stage_in_recalculation.call();
    // //  console.log("Stage in recalculation:",result.toNumber());
    //   //assert.equal(result.toNumber(),0,'Payment doesn\'t close stage 3');
    // });

    it("Bonus count", async () =>{
      let moni = await MoniContract.deployed();
      let result = await moni.bonus_count.call();
      console.log("Bonus count:",result.toNumber());
      //assert.equal(result.toNumber(),0,'Payment doesn\'t close stage 3');
    });

    it("Get vesting index and if % 2 it withdraws bonus from contract", async () =>{
      let moni = await MoniContract.deployed();
      let token = await Token.deployed()
      let result = await moni.vesting_date_count.call();
      console.log("Get vesting index before calculate:",result.toNumber());
      let unlocked_amount = await moni._investors.call(firstInvestor);
      //assert.equal(result.toNumber(),0,'Payment doesn\'t close stage 3');
      if (result.toNumber() % 2 == 0) {
        console.log("Unlocked amount:",unlocked_amount[5].toNumber());
        let token_withdraw = await moni.tokens_withdraw(web3.toWei(1,"ether"), {from: firstInvestor});
        let tokens_withdraw_admin = await moni.tokens_withdraw_admin(firstInvestor, web3.toWei(1,"ether"), {from: firstAdmin});
        let token_balance = await token.balanceOf(firstInvestor);
        console.log(unlocked_amount[5].toNumber());
        console.log(token_balance.toNumber());
      }
    });

    it("Get unlocked_amount from first Investor (after withdraw)", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance._investors.call(firstInvestor,{from:firstAdmin});
      })
      .then(result => {
      //  console.log("Result of investor:",result);
        console.log("Unlocked amount:",result[5].toNumber());
        console.log("Total withdraw:",result[6].toNumber());
        console.log("Locked amount:",result[4].toNumber());
      })
    });
    it("Get unlocked_amount from first stage (after withdraw)", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance._stage_invest.call(1,firstInvestor,{from:firstAdmin});
      })
      .then(result => {
      //  console.log("Result of investor:",result);
        console.log("Unlocked amount stage 1:",result[1].toNumber());
      })
    });

    it("Get dividends in this stage", () => {
      return MoniContract.deployed()
      .then(instance => {
        return instance._stage_invest.call(1,firstInvestor,{from:firstAdmin});
      })
      .then(result =>{
        console.log("Devidents",result[2].toNumber());
      });
     });

}

});
