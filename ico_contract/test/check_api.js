var MoniContract = artifacts.require("./MoniContract.sol");
const axios = require('axios');
//for (i=1;i<7;i++) {
//  console.log('TEST FOR STAGE:',i);
contract('MoniContract', (accounts) => {

  var STAGE = 1;
  var ETHER_FOR_INVEST;
  var GET_TOKENS_AMOUNT;
  var DURATION = 12000;
  var MAX = 60;
  var MIN = 12;
  var MAX_TOKENS_INVEST = web3.toWei(600,"ether");
  var AMOUNT_IN_ETHER = 0.09; //12$
  var AMOUNT_IN_ETHER_MIN = 0.01
  var AMOUNT_IN_ETHER_MAX = 0.11
  var TOKEN_FOR_INVESTOR = web3.toWei(300,"ether");
  var TOKEN_FOR_INVESTOR_OVER = web3.toWei(601,"ether");
  var BONUS_IN_PERSENT = 50;
  var BITCOIN_AMOUNT = (0.0018 * 100000000); //in btc
  var BITCOIN_AMOUNT_MORE = (0.08 * 100000000); //in btc
  var TOKENS_FROM_BITCOIN = web3.toWei((BITCOIN_AMOUNT*4*100000/100000000),"ether");

  var creatorAddress = accounts[0];
  var firstAdmin = accounts[1];
  var seconAdmin = accounts[2];
  var firstInvestor = accounts[3];
  var secondInvestor = accounts[4];

//Test of creation of Investors
  it("Set addmin for testing", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_admin_initial(firstAdmin,{from:creatorAddress});
    })
  });

  it("Investor registration", () => {
    return MoniContract.deployed()
    .then(instance => {
      return instance.set_investor(firstInvestor,{from:firstAdmin})
    })
  });

});
