odoo.define('imq.buttons', function (require) {
"use strict";

var FormController = require('web.FormController');

FormController.include({
	_onButtonClicked: function (event) {
			if(event.data.attrs.custom === "click"){
				$(".o_form_button_edit").click();
			    setTimeout(5000);
			    $(".o_field_x2many_list_row_add a").click();
			} else { this._super(event);}
			},
	});

});
