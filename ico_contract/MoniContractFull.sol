pragma solidity ^0.4.24;


contract DateTime {
        /*
         *  Date and Time utilities for ethereum contracts
         *
         */
        struct _DateTime {
                uint16 year;
                uint8 month;
                uint8 day;
                uint8 hour;
                uint8 minute;
                uint8 second;
                uint8 weekday;
        }

        uint constant DAY_IN_SECONDS = 86400;
        uint constant YEAR_IN_SECONDS = 31536000;
        uint constant LEAP_YEAR_IN_SECONDS = 31622400;

        uint constant HOUR_IN_SECONDS = 3600;
        uint constant MINUTE_IN_SECONDS = 60;

        uint16 constant ORIGIN_YEAR = 1970;

        function isLeapYear(uint16 year) public pure returns (bool) {
                if (year % 4 != 0) {
                        return false;
                }
                if (year % 100 != 0) {
                        return true;
                }
                if (year % 400 != 0) {
                        return false;
                }
                return true;
        }

        function leapYearsBefore(uint year) public pure returns (uint) {
                year -= 1;
                return year / 4 - year / 100 + year / 400;
        }

        function getDaysInMonth(uint8 month, uint16 year) public pure returns (uint8) {
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                        return 31;
                }
                else if (month == 4 || month == 6 || month == 9 || month == 11) {
                        return 30;
                }
                else if (isLeapYear(year)) {
                        return 29;
                }
                else {
                        return 28;
                }
        }

        function parseTimestamp(uint timestamp) internal pure returns (_DateTime dt) {
                uint secondsAccountedFor = 0;
                uint buf;
                uint8 i;

                // Year
                dt.year = getYear(timestamp);
                buf = leapYearsBefore(dt.year) - leapYearsBefore(ORIGIN_YEAR);

                secondsAccountedFor += LEAP_YEAR_IN_SECONDS * buf;
                secondsAccountedFor += YEAR_IN_SECONDS * (dt.year - ORIGIN_YEAR - buf);

                // Month
                uint secondsInMonth;
                for (i = 1; i <= 12; i++) {
                        secondsInMonth = DAY_IN_SECONDS * getDaysInMonth(i, dt.year);
                        if (secondsInMonth + secondsAccountedFor > timestamp) {
                                dt.month = i;
                                break;
                        }
                        secondsAccountedFor += secondsInMonth;
                }

                // Day
                for (i = 1; i <= getDaysInMonth(dt.month, dt.year); i++) {
                        if (DAY_IN_SECONDS + secondsAccountedFor > timestamp) {
                                dt.day = i;
                                break;
                        }
                        secondsAccountedFor += DAY_IN_SECONDS;
                }

                // // Hour
                // dt.hour = getHour(timestamp);

                // // Minute
                // dt.minute = getMinute(timestamp);

                // // Second
                // dt.second = getSecond(timestamp);

                // // Day of week.
                // dt.weekday = getWeekday(timestamp);
        }

        function getYear(uint timestamp) public pure returns (uint16) {
                uint secondsAccountedFor = 0;
                uint16 year;
                uint numLeapYears;

                // Year
                year = uint16(ORIGIN_YEAR + timestamp / YEAR_IN_SECONDS);
                numLeapYears = leapYearsBefore(year) - leapYearsBefore(ORIGIN_YEAR);

                secondsAccountedFor += LEAP_YEAR_IN_SECONDS * numLeapYears;
                secondsAccountedFor += YEAR_IN_SECONDS * (year - ORIGIN_YEAR - numLeapYears);

                while (secondsAccountedFor > timestamp) {
                        if (isLeapYear(uint16(year - 1))) {
                                secondsAccountedFor -= LEAP_YEAR_IN_SECONDS;
                        }
                        else {
                                secondsAccountedFor -= YEAR_IN_SECONDS;
                        }
                        year -= 1;
                }
                return year;
        }

        function getMonth(uint timestamp) public pure returns (uint8) {
                return parseTimestamp(timestamp).month;
        }

        function getDay(uint timestamp) public pure returns (uint8) {
                return parseTimestamp(timestamp).day;
        }

        // function getHour(uint timestamp) public pure returns (uint8) {
        //         return uint8((timestamp / 60 / 60) % 24);
        // }

        // function getMinute(uint timestamp) public pure returns (uint8) {
        //         return uint8((timestamp / 60) % 60);
        // }

        // function getSecond(uint timestamp) public pure returns (uint8) {
        //         return uint8(timestamp % 60);
        // }

        // function getWeekday(uint timestamp) public pure returns (uint8) {
        //         return uint8((timestamp / DAY_IN_SECONDS + 4) % 7);
        // }

        // function toTimestamp(uint16 year, uint8 month, uint8 day) public pure returns (uint timestamp) {
        //         return toTimestamp(year, month, day, 0, 0, 0);
        // }

        // function toTimestamp(uint16 year, uint8 month, uint8 day, uint8 hour) public pure returns (uint timestamp) {
        //         return toTimestamp(year, month, day, hour, 0, 0);
        // }

        // function toTimestamp(uint16 year, uint8 month, uint8 day, uint8 hour, uint8 minute) public pure returns (uint timestamp) {
        //         return toTimestamp(year, month, day, hour, minute, 0);
        // }

        // function toTimestamp(uint16 year, uint8 month, uint8 day, uint8 hour, uint8 minute, uint8 second) public pure returns (uint timestamp) {
        //         uint16 i;

        //         // Year
        //         for (i = ORIGIN_YEAR; i < year; i++) {
        //                 if (isLeapYear(i)) {
        //                         timestamp += LEAP_YEAR_IN_SECONDS;
        //                 }
        //                 else {
        //                         timestamp += YEAR_IN_SECONDS;
        //                 }
        //         }

        //         // Month
        //         uint8[12] memory monthDayCounts;
        //         monthDayCounts[0] = 31;
        //         if (isLeapYear(year)) {
        //                 monthDayCounts[1] = 29;
        //         }
        //         else {
        //                 monthDayCounts[1] = 28;
        //         }
        //         monthDayCounts[2] = 31;
        //         monthDayCounts[3] = 30;
        //         monthDayCounts[4] = 31;
        //         monthDayCounts[5] = 30;
        //         monthDayCounts[6] = 31;
        //         monthDayCounts[7] = 31;
        //         monthDayCounts[8] = 30;
        //         monthDayCounts[9] = 31;
        //         monthDayCounts[10] = 30;
        //         monthDayCounts[11] = 31;

        //         for (i = 1; i < month; i++) {
        //                 timestamp += DAY_IN_SECONDS * monthDayCounts[i - 1];
        //         }

        //         // Day
        //         timestamp += DAY_IN_SECONDS * (day - 1);

        //         // Hour
        //         timestamp += HOUR_IN_SECONDS * (hour);

        //         // Minute
        //         timestamp += MINUTE_IN_SECONDS * (minute);

        //         // Second
        //         timestamp += second;

        //         return timestamp;
        // }
}


// Ethereum Token callback
interface tokenRecipient {
  function receiveApproval( address from, uint256 value, bytes data ) external;
}

// ERC223 callback
interface ContractReceiver {
  function tokenFallback( address from, uint value, bytes data ) external;
}

contract owned {
  address public owner;

  function owned() public {
    owner = msg.sender;
  }

  function changeOwner( address _miner ) public onlyOwner {
    owner = _miner;
  }

  modifier onlyOwner {
    require (msg.sender == owner);
    _;
  }
}

// ERC20 token with added ERC223 and Ethereum-Token support
//
// Blend of multiple interfaces:
// - https://theethereum.wiki/w/index.php/ERC20_Token_Standard
// - https://www.ethereum.org/token (uncontrolled, non-standard)
// - https://github.com/Dexaran/ERC23-tokens/blob/Recommended/ERC223_Token.sol

contract MONIToken is owned {

  string  public name;
  string  public symbol;
  uint8   public decimals;
  uint256 public totalSupply;
  uint256 public supplyCap;

  uint256 public noTransferBefore;

  mapping( address => uint256 ) balances_;
  mapping( address => mapping(address => uint256) ) allowances_;

  // ERC20
  event Approval( address indexed owner,
                  address indexed spender,
                  uint value );

  // ERC20-compatible version only, breaks ERC223 compliance but etherscan
  // and exchanges only support ERC20 version. Can't overload events

  event Transfer( address indexed from,
                  address indexed to,
                  uint256 value );
                  //bytes    data );

  // Ethereum Token
  event Burn( address indexed from,
              uint256 value );

  function MONIToken( uint8 _decimals,
                          uint256 _tokcap,
                          string _name,
                          string _symbol,
                          uint256 _noTransferBefore // datetime, in seconds
  ) public {

    decimals = uint8(_decimals); // audit recommended 18 decimals
    supplyCap = _tokcap * 10**uint256(decimals);
    totalSupply = 0;

    name = _name;
    symbol = _symbol;
    noTransferBefore = _noTransferBefore;
  }

  function mine( uint256 qty ) public onlyOwner {
    require (    (totalSupply + qty) > totalSupply
              && (totalSupply + qty) <= supplyCap
            );

    totalSupply += qty;
    balances_[owner] += qty;
    emit Transfer( address(0), owner, qty );
  }

  function cap() public constant returns(uint256) {
    return supplyCap;
  }

  // ERC20
  function balanceOf( address owner ) public constant returns (uint) {
    return balances_[owner];
  }

  // ERC20
  function approve( address spender, uint256 value ) public
  returns (bool success)
  {
    // WARNING! When changing the approval amount, first set it back to zero
    // AND wait until the transaction is mined. Only afterwards set the new
    // amount. Otherwise you may be prone to a race condition attack.
    // See: https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729

    allowances_[msg.sender][spender] = value;
    emit Approval( msg.sender, spender, value );
    return true;
  }

  // recommended fix for known attack on any ERC20
  function safeApprove( address _spender,
                        uint256 _currentValue,
                        uint256 _value ) public
  returns (bool success)
  {
    // If current allowance for _spender is equal to _currentValue, then
    // overwrite it with _value and return true, otherwise return false.

    if (allowances_[msg.sender][_spender] == _currentValue)
      return approve(_spender, _value);

    return false;
  }

  // ERC20
  function allowance( address owner, address spender ) public constant
  returns (uint256 remaining)
  {
    return allowances_[owner][spender];
  }

  // ERC20
  function transfer(address to, uint256 value) public
  returns (bool success)
  {
    bytes memory empty; // null
    _transfer( msg.sender, to, value, empty );
    return true;
  }

  // ERC20
  function transferFrom( address from, address to, uint256 value ) public
  returns (bool success)
  {
    require( value <= allowances_[from][msg.sender] );

    allowances_[from][msg.sender] -= value;
    bytes memory empty;
    _transfer( from, to, value, empty );

    return true;
  }

  // Ethereum Token
  function approveAndCall( address spender,
                           uint256 value,
                           bytes context ) public
  returns (bool success)
  {
    if ( approve(spender, value) )
    {
      tokenRecipient recip = tokenRecipient( spender );

      if (isContract(recip))
        recip.receiveApproval( msg.sender, value, context );

      return true;
    }

    return false;
  }

  // Ethereum Token
  function burn( uint256 value ) public
  returns (bool success)
  {
    require( balances_[msg.sender] >= value );
    balances_[msg.sender] -= value;
    totalSupply -= value;

    emit Burn( msg.sender, value );
    return true;
  }

  // Ethereum Token
  function burnFrom( address from, uint256 value ) public
  returns (bool success)
  {
    require( balances_[from] >= value );
    require( value <= allowances_[from][msg.sender] );

    balances_[from] -= value;
    allowances_[from][msg.sender] -= value;
    totalSupply -= value;

    emit Burn( from, value );
    return true;
  }

  // ERC223 Transfer and invoke specified callback
  function transfer( address to,
                     uint value,
                     bytes data,
                     string custom_fallback ) public returns (bool success)
  {
    _transfer( msg.sender, to, value, data );

    // throws if custom_fallback is not a valid contract call
    require( address(to).call.value(0)(bytes4(keccak256(custom_fallback)),
             msg.sender,
             value,
             data) );

    return true;
  }

  // ERC223 Transfer to a contract or externally-owned account
  function transfer( address to, uint value, bytes data ) public
  returns (bool success)
  {
    if (isContract(to)) {
      return transferToContract( to, value, data );
    }

    _transfer( msg.sender, to, value, data );
    return true;
  }

  // ERC223 Transfer to contract and invoke tokenFallback() method
  function transferToContract( address to, uint value, bytes data ) private
  returns (bool success)
  {
    _transfer( msg.sender, to, value, data );

    ContractReceiver rx = ContractReceiver(to);

    if (isContract(rx)) {
      rx.tokenFallback( msg.sender, value, data );
      return true;
    }

    return false;
  }

  // ERC223 fetch contract size (must be nonzero to be a contract)
  function isContract( address _addr ) private constant returns (bool)
  {
    uint length;
    assembly { length := extcodesize(_addr) }
    return (length > 0);
  }

  function _transfer( address from,
                      address to,
                      uint value,
                      bytes data ) internal
  {
    require( to != 0x0 );
    require( balances_[from] >= value );
    require( balances_[to] + value > balances_[to] ); // catch overflow

    // no transfers allowed before trading begins
    if (msg.sender != owner) require( now >= noTransferBefore );

    balances_[from] -= value;
    balances_[to] += value;

    bytes memory ignore;
    ignore = data;                    // ignore compiler warning
    emit Transfer( from, to, value ); // ignore data
  }
}




contract MoniContract is DateTime {
// DECLARATION OF EVENTS ======================= start =========================
event Payment( string _bitcoin,
                   address _from,
                   uint _date,
                   uint _ether_amount,
                   uint _btc_amount,
                   uint _tokens,
                   uint _round,
                   string _type);
// DECLARATION OF VARIABLES ==================== start =========================

//constructor for hardcoding of constrant values in contract

    constructor(address _token_address) public {
        owner = msg.sender;
        token_address = _token_address;
        set_stage_initial();
        set_admin_initial();
        set_owners_initial();


    }

//VARIABLES
address owner;
uint public current_stage;
uint public stage_count;
address public token_address;
uint public token_to_cent = 10; //tokens to cent
uint public token_rate = 6000; //rate for ether to tokens
uint public bitcoin_rate = 100000; //rate for bitcoin to tokens
//uint public current_date = now;
uint public calculate_date;
uint public current_date;
bool public ico_end;
uint public end_ico_year;
uint public end_ico_month;
uint public end_ico_day;
uint public stop_contract_state;
uint public vesting_date_count;
uint public bonus_count;
uint public stage_in_recalculation;
MONIToken public MONIcoin;
uint public general_limit_stage = 1000000 * (1 ether);
//HARD CODING

/* uint public _bonus1 = 50;
uint public _vesting_time1 = 9;
uint public total_tokens1 = 12500000 * (1 ether);
uint public _max1 = 1000000;
uint public _min1 = 100000; */



function get_current_date() internal returns (uint) {
  return current_date;
}

function set_general_limit(uint _general_limit_stage) public OnlyAdmin(msg.sender) {
  general_limit_stage = _general_limit_stage * (1 ether);
}

// Investors struct - write here when signup of investor
        struct Investors {
            uint total_invest;
            uint total_invest_ether;
            uint total_invest_btc;
            bool active;
            uint locked_amount;
            uint unlocked_amount;
            uint total_withdraw;
            uint signup;

        }

        mapping(address => Investors) public _investors;

//struct for stage

    struct stage {
        bool active;
        uint start_date;
        uint bonus; //bonus in percent
        uint duration;
        uint sold_token;
        uint vesting_time;
        uint min;
        uint max;
        uint total_tokens;
        uint end_date;
        uint total_investors;

    }

    mapping(uint=>stage) public _stage;
function set_stage_initial() internal {
_stage[1].vesting_time =9;
_stage[1].bonus = 0;
_stage[1].total_tokens = 3125000 * (1 ether);
_stage[1].max = 1000000;
_stage[1].min = 100000;

_stage[2].bonus = 30;
_stage[2].vesting_time = 9;
_stage[2].total_tokens = 12500000 * (1 ether);
_stage[2].max = 1000000;
_stage[2].min = 50000;

_stage[3].bonus = 20;
_stage[3].vesting_time = 9;
_stage[3].total_tokens = 25000000 * (1 ether);
_stage[3].max = 1000000;
_stage[3].min = 20000;

_stage[4].bonus = 15;
_stage[4].vesting_time = 6;
_stage[4].total_tokens = 12500000 * (1 ether);
_stage[4].min = 1000;
_stage[4].max = 15000;

_stage[5].bonus = 10;
_stage[5].vesting_time = 6;
_stage[5].total_tokens = 18750000 * (1 ether);
_stage[5].min = 1000;
_stage[5].max = 3000;


_stage[6].bonus = 5;
_stage[6].vesting_time = 6;
_stage[6].total_tokens = 56250000 * (1 ether);
_stage[6].min = 1000;
_stage[6].max = 3000;


_stage[7].bonus = 0;
_stage[7].vesting_time = 6;
_stage[7].total_tokens = 56250000 * (1 ether);
_stage[7].min = 1000;
_stage[7].max = 3000;

_stage[8].bonus = 0;
_stage[8].vesting_time = 12;
_stage[8].total_tokens = 52000000 * (1 ether);
_stage[8].min = 1000;
_stage[8].max = 3000;
}
//struct for investing in stages

    struct stage_invest {
        uint locked_amount;
        uint unlocked_amount;
        uint dividends;
        uint total_invest_stage;
        uint has_payments;
    }

    mapping(uint=>mapping(address=>stage_invest)) public _stage_invest;  // _stage_invest[stage][address]
    mapping(uint=>mapping(uint => address)) public _stage_invest_index;  //for iterations _stage_invest_index(address)[stage][index]
    mapping(uint=>uint) public _stage_count;                             //count in stage _stage_count[stage][number_of_records]

    struct Admins {
      bool active;
    }

    mapping(address=>Admins) public  _admins;

   struct Owners {
     bool active;
   }

   mapping(address=>Owners) public _owners;
// DECLARATION OF VARIABLES ==================== end ===========================


//##############################################################################
//                                 MODIFIERS
//##############################################################################
modifier OnlyOwner(address _sender_address) {
  require(_owners[_sender_address].active == true);
  _;

}
modifier OnlyAdmin(address _admin_address) {
  require(_admins[_admin_address].active == true);
  _;
}

modifier StopContract(uint _stop_contract_state) {
  require(_stop_contract_state == 0);
  _;
}

modifier InvestorCheck(address _investor_address_check) {
  require(_investors[_investor_address_check].active == true);
  _;
}
//set admin active or not ====================== start =========================
function set_rate(uint _token_rate, uint _bitcoin_rate, uint _token_cent) {
  token_rate = _token_rate;
  bitcoin_rate = _bitcoin_rate;
  token_to_cent = _token_cent;
}
//test function for setting Admins
function set_admin_initial() internal {
  _admins[0x22346eC7cC90Dfd971a92c731904B79e0bEc302E].active = true;
  _admins[0x38a22A715753eA01533930dF3cb112FdBcd0a9AB].active = true;
  _admins[0xaAd34d37DBb5a91d25c651080D147dF59e35A957].active = true;
  _admins[0xa14D47b43D92F4F0e2f474c2b059D0C2000318BF].active = true;
  _admins[0x54e7d4954b2e0c4BbEa6E55B5F89a6A6359b695C].active = true;
  _admins[0xf71260be31dc3c437dd90f91e7d71eaaaa4e0a8e].active = true;
}

function set_owners_initial() internal {
  _owners[0xa14D47b43D92F4F0e2f474c2b059D0C2000318BF].active = true;
  _owners[0xaAd34d37DBb5a91d25c651080D147dF59e35A957].active = true;
}

function set_admin(address _admin_address, bool _active) public
    OnlyOwner(msg.sender)
    returns (bool)
    {
      //require(_admins[_admin_address].active == true);
      _admins[_admin_address].active = _active;

    }

//SET INVESTOR ========================= start =================================
   function set_investor(address _address) public
     OnlyAdmin(msg.sender)
     StopContract(stop_contract_state)
     returns (string)
     {
       if (_investors[_address].signup != 1) {
           _investors[_address].active = true;
           _investors[_address].signup = 1;
           return "Singup OK";
       }
       else
       {
         return "Singed up allready";
       }
  }

  function set_investor_active(address _investor_address, bool _investor_active)
                              public
                              OnlyAdmin(msg.sender)
      {
        _investors[_investor_address].active = _investor_active;
        if (_investor_active == true) {
          emit Payment( '',
                  _investor_address,
                  current_date,
                  0,
                  0,
                  0,
                  0,
                  "investor_true");
        } else {
          emit Payment( '',
                  _investor_address,
                  current_date,
                  0,
                  0,
                  0,
                  0,
                  "investor_false");
        }


      }
//SET INVESTOR ========================= end ===================================


//SETTING OF STAGE ============================= start =========================

    function set_current_stage(uint _current_stage,
                              uint _duration, uint _min, uint _max)
                              public
                              OnlyAdmin(msg.sender)
                              StopContract(stop_contract_state)
                              returns(uint)
    {
        require(_current_stage == stage_count+1 && _current_stage <= 8);

        _stage[_current_stage].start_date = get_current_date();

        _stage[_current_stage].duration = _duration;
        _stage[_current_stage].min = _min;
        _stage[_current_stage].max = _max;
        _stage[_current_stage].active = true;
        stage_count += 1;
        current_stage = _current_stage;
        return current_stage;
    }

//PAYMENTS ALL WAYS ========================= start ============================
//when we get ether from investors
   function ()
     payable public
     StopContract(stop_contract_state)
     InvestorCheck(msg.sender)
     {
     require(_investors[msg.sender].signup == 1);
     require(current_stage != 8);
     uint _tokens = msg.value*token_rate;
     require(_investors[msg.sender].total_invest + _tokens <= general_limit_stage);
     //uint _investing_amount;
     require(RequireGetPayment(_tokens) == true);
     uint _current_stage = current_stage;
     var (_investing_amount) = AddPayment(msg.sender, _tokens );
     _investors[msg.sender].total_invest_ether += msg.value;
     emit Payment( '',
             msg.sender,
             current_date,
             msg.value,
             0,
             _investing_amount,
             _current_stage,
             'from_ether');


   }


   //when we get ether from investors
      function add_ether_to_investor(address _investors_address, uint _ether_value) public
        payable
        OnlyAdmin(msg.sender)
        StopContract(stop_contract_state)
        InvestorCheck(msg.sender)
        {
        require(_investors[_investors_address].signup == 1);
        require(current_stage != 8);
        uint _curent_stage = current_stage;
        uint _tokens = _ether_value*token_rate;
        require(_investors[_investors_address].total_invest + _tokens <= general_limit_stage);
        //uint _investing_amount;
        require(RequireGetPayment(_tokens) == true);
        var (_investing_amount) = AddPayment(_investors_address, _tokens );
        _investors[_investors_address].total_invest_ether += _ether_value;
       emit Payment( '',
                msg.sender,
                current_date,
                msg.value,
                0,
                _investing_amount,
                _curent_stage,
                'add_ether');


      }
   //set tokens for investor

    function add_tokens_to_investor(address _address, uint _tokens_for_add) public
        OnlyAdmin(msg.sender)
        StopContract(stop_contract_state)
        InvestorCheck(msg.sender)
        {
        require(_investors[_address].signup == 1 && _investors[_address].active == true);
        require(_investors[_address].total_invest + _tokens_for_add <= general_limit_stage);
        require(RequireGetPayment(_tokens_for_add) == true);
        uint _current_stage = current_stage;
        //uint _investing_amount;
        var _investing_amount = AddPayment(_address,  _tokens_for_add);
        emit Payment( '',
                 _address,
                 current_date,
                 0,
                 0,
                 _investing_amount,
                 _current_stage,
                 'add_tokens');
    }



//function add tokens from bitcoin
function give_tokens_bitcoin(address _investors_address,
                             uint _bitcoin_amount,
                             string _from_bitcoin_address) public
                             OnlyAdmin(msg.sender)
                             StopContract(stop_contract_state)
                             InvestorCheck(msg.sender)
      {
        require(current_stage != 8);
        require(_investors[_investors_address].signup == 1 && _investors[_investors_address].active == true);
        uint _btc_tokens = (_bitcoin_amount*bitcoin_rate)/100000000;
        require(_investors[_investors_address].total_invest + _btc_tokens <= general_limit_stage);
        _btc_tokens = _btc_tokens * (1 ether);
        require(RequireGetPayment(_btc_tokens) == true);
        uint _current_stage = current_stage;
        //uint _investing_amount;
        var _investing_amount = AddPayment(_investors_address, _btc_tokens);

        _investors[_investors_address].total_invest_btc += _bitcoin_amount;

        emit Payment( _from_bitcoin_address,
                 _investors_address,
                 current_date,
                 0,
                 _bitcoin_amount,
                 _investing_amount,
                 _current_stage,
                 'from_btc');

    }
//function for checking of posibility of payment at this time
function RequireGetPayment(uint _bonus) internal  returns(bool) {
     require(current_stage < 9 && current_stage != 0);
     uint current_date_now;
     current_date_now = get_current_date();
     //uint current_date_now = block.timestamp;
     uint _in_dollars = _bonus/token_to_cent/(1 ether);
     bool _difference_more;
     bool _min_max;
     bool _date_check;
     // check of difference sold_token + _bonus and sold_token

      if (_stage[current_stage].max >= _in_dollars && _stage[current_stage].min <= _in_dollars) {
          _min_max = true;
      }
     //check date if stage exp. = stop
      if (current_date_now < (_stage[current_stage].start_date + _stage[current_stage].duration)) {
          _date_check = true;
      }

      if (_min_max == true && _date_check == true) {

          return true;
      } else
      {
        return false;
      }

  }

  //function of payemnts for all ways
      function AddPayment( address _address,
                           uint _tokens)
                           internal
                           returns(uint)
      {
          // check if this investor had payments in current stage
          if (_stage_invest[current_stage][_address].has_payments == 0)
          {
              _stage[current_stage].total_investors += 1;  //increment of total investors in stage
              _stage_invest_index[current_stage][_stage[current_stage].total_investors] = _address;
              _stage_invest[current_stage][_address].has_payments = 1;  //save event of payment for next time
          }
          _stage[current_stage].sold_token += _tokens;
          //calculate tokens with bonuses (bonus in persent)
          uint bonus = _stage[current_stage].bonus;
          uint investing_amount = _tokens + _tokens/100*bonus;
          _stage_invest[current_stage][_address].locked_amount += investing_amount;
          _stage_invest[current_stage][_address].total_invest_stage += investing_amount;
          _investors[_address].total_invest += investing_amount;
          _investors[_address].locked_amount += investing_amount;

           if (_stage[current_stage].sold_token >= _stage[current_stage].total_tokens) {
              _stage[current_stage].active = false;
              if (current_stage == 7) {
                  ico_end = true;
                  end_ico_year = getYear(current_date);
                  end_ico_month = getMonth(current_date);
                  end_ico_day = getDay(current_date);
                  }
              current_stage = 0;
          }
           return investing_amount;

      }
//PAYMENTS ALL WAYS ========================= end ++============================
//stop contract

function stop_contract(uint _stop_contract) OnlyOwner(msg.sender) {
  stop_contract_state = _stop_contract;
}
//CRON START ================================ start ============================
 function get_month_from_ico_end() public view returns(uint months_from_end_ico) {
     if (getYear(current_date) == end_ico_year)
     {
     months_from_end_ico = getMonth(current_date) - end_ico_month;
     }
     else
     {
     months_from_end_ico = getMonth(current_date)+(getYear(current_date)-(end_ico_year+1))*12 + (12-end_ico_month);
     }
  }

function Calculate() public OnlyAdmin(msg.sender) returns(uint stage_now, bool recalculate) {
    if (current_stage != 0) {
       if ((current_date - _stage[current_stage].start_date) >= _stage[current_stage].duration) {
            if (current_stage == 7 && ico_end != true) {
                ico_end = true;
                end_ico_year = getYear(current_date);
                end_ico_month = getMonth(current_date);
                end_ico_day = getDay(current_date);

            }
            _stage[current_stage].active = false;
            current_stage = 0;
        }
    }

recalculate = false;
if (ico_end == true) {
  bool _can_go;
  uint months_from_ico_end = get_month_from_ico_end();
  if ((months_from_ico_end - vesting_date_count) == 1 && getDay(current_date) >= end_ico_day) {
    _can_go = true;
  }
  if ((months_from_ico_end - vesting_date_count) > 1) {
    _can_go = true;
  }
    bool plus_persent;
    uint stage_in;
  if (months_from_ico_end != vesting_date_count && _can_go == true) {
        vesting_date_count += 1;
  //not calculate when it was ran
    uint stage_for_calculation;
    if ((vesting_date_count - bonus_count) % 2 == 0)
    {
      plus_persent = true;
      bonus_count = months_from_ico_end;
    } else { plus_persent = false; }

//stage cicle
    for(uint s=1; s<9; s++){
        uint vesting_time_in_stage = _stage[s].vesting_time;


if (_stage[s].total_investors != 0) {
        for (uint j=1; j<=_stage[s].total_investors; j++) {

       if ( months_from_ico_end >= vesting_time_in_stage && _stage_invest[s][_stage_invest_index[s][j]].locked_amount > 0)
         {
           _stage_invest[s][_stage_invest_index[s][j]].unlocked_amount += _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;
           _stage_invest[s][_stage_invest_index[s][j]].locked_amount -= _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;
           _investors[_stage_invest_index[s][j]].unlocked_amount += _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;
           _investors[_stage_invest_index[s][j]].locked_amount -= _stage_invest[s][_stage_invest_index[s][j]].total_invest_stage/10;

         }

         if (plus_persent == true) {
          if((_stage_invest[s][_stage_invest_index[s][j]].unlocked_amount + _stage_invest[s][_stage_invest_index[s][j]].locked_amount) > 0)
             {
             uint dividents = (_stage_invest[s][_stage_invest_index[s][j]].unlocked_amount + _stage_invest[s][_stage_invest_index[s][j]].locked_amount)/100;
             _stage_invest[s][_stage_invest_index[s][j]].dividends += dividents;
             _stage_invest[s][_stage_invest_index[s][j]].unlocked_amount += dividents;
             _investors[_stage_invest_index[s][j]].unlocked_amount += dividents;
             }
          }
      }
}
//list of investors from stage
   }
//stage cicle
  }
}

    stage_now = current_stage;
}


function tokens_withdraw(uint _tokens_amount) public returns(string) {
  require(_investors[msg.sender].unlocked_amount >= _tokens_amount);
  MONIToken(token_address).transfer(msg.sender,_tokens_amount);
  uint withdraw_date = get_current_date();
  _investors[msg.sender].unlocked_amount -= _tokens_amount;
  _investors[msg.sender].total_withdraw += _tokens_amount;
  uint _tokens_amount_event = _tokens_amount;
  bool stop;
  for (uint i = 1;i<9;i++) {
    if (_stage_invest[i][msg.sender].unlocked_amount != 0 && _tokens_amount != 0) {
      if (_stage_invest[i][msg.sender].unlocked_amount >= _tokens_amount) {
      _stage_invest[i][msg.sender].unlocked_amount -= _tokens_amount;
      _tokens_amount = 0;
      }
      else {
        _tokens_amount = _tokens_amount - _stage_invest[i][msg.sender].unlocked_amount;
        _stage_invest[i][msg.sender].unlocked_amount = 0;
      }
    }
  }

  emit Payment( '',
           msg.sender,
           current_date,
           0,
           0,
           _tokens_amount_event,
           0,
           'withdraw');
  return "Withdraw OK";
}

function tokens_withdraw_admin(address _to,uint _tokens_amount)
  public
  OnlyAdmin(msg.sender)
  {
    require(_investors[_to].unlocked_amount >= _tokens_amount);
    MONIToken(token_address).transfer(_to,_tokens_amount);
    uint withdraw_date = get_current_date();
    _investors[_to].unlocked_amount -= _tokens_amount;
    _investors[_to].total_withdraw += _tokens_amount;
    uint _tokens_amount_event = _tokens_amount;
    for (uint i = 1;i<9;i++) {
      if (_stage_invest[i][msg.sender].unlocked_amount != 0 && _tokens_amount != 0) {
        if (_stage_invest[i][msg.sender].unlocked_amount >= _tokens_amount) {
        _stage_invest[i][msg.sender].unlocked_amount -= _tokens_amount;
        _tokens_amount = 0;
        }
        else {
          _tokens_amount = _tokens_amount - _stage_invest[i][msg.sender].unlocked_amount;
          _stage_invest[i][msg.sender].unlocked_amount = 0;
        }
      }
    }

    emit Payment( '',
             _to,
             current_date,
             0,
             0,
             _tokens_amount_event,
             0,
             'withdraw_a');
  }

function withdraw_ether(uint _ether) public OnlyOwner(msg.sender) {
  //require(ico_end == true);
  require(address(this).balance >= _ether);
  msg.sender.transfer(_ether);
}

function refund_investor(address _investors_address)  public OnlyAdmin(msg.sender) {
  require(ico_end == true);
  uint _total_invest_ether =  _investors[_investors_address].total_invest_ether;
  if (_total_invest_ether != 0 ) {
        _investors_address.transfer(_investors[_investors_address].total_invest_ether);
        _investors[_investors_address].total_invest_ether = 0;
      }

      emit Payment( '',
               _investors_address,
               current_date,
               _total_invest_ether,
               0,
               0,
               0,
               'refund_investor');
    }

function refund_all()  public  OnlyOwner(msg.sender) returns (bool) {
  for(uint s=1; s<9; s++)
  {
    for (uint j=1; j<=_stage[s].total_investors; j++)
    {
        if(_investors[_stage_invest_index[s][j]].total_invest_ether != 0 )
        {
          address investor = _stage_invest_index[s][j];

investor.transfer(_investors[_stage_invest_index[s][j]].total_invest_ether);

emit Payment( '',
         _stage_invest_index[s][j],
         current_date,
         _investors[_stage_invest_index[s][j]].total_invest_ether,
         0,
         0,
         0,
         'refund_investor');

_investors[_stage_invest_index[s][j]].total_invest_ether = 0;

        }
    }
  }
}


function SetCurrentDate(uint _current_date) public returns(uint){
        current_date = _current_date;
        return current_date;
    }

}
